import logging
import string
from datetime import datetime, timedelta

import telegram as tg
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

import pyowm
from pyowm.webapi25.forecaster import Forecaster
from pyowm.exceptions.api_call_error import APICallError
from pyowm.webapi25.weather import Weather


class WeatherGetter:
    def __init__(self):
        with open('owm.token') as f:
            owm_token = f.read()
        self.owm = pyowm.OWM(owm_token, language='ru')

    def get_time(self, words):
        word_set = set(words)

        now = datetime.utcnow()

        message_weekday_set = word_set.intersection(self.weekdays)

        if message_weekday_set:
            weekday = message_weekday_set.pop()
            weekday_num = self.weekdays[weekday]

            day_diff = weekday_num - now.weekday()

            if day_diff < 0:
                day_diff += 7

            verbal = self.verbal_weekdays[weekday_num]
        elif 'завтра' in words:
            day_diff = 1
            verbal = 'Завтра'
        elif 'послезавтра' in words:
            day_diff = 2
            verbal = 'Послезавтра'
        else:
            day_diff = 1
            verbal = 'Завтра'

        target_date = (now + timedelta(days=day_diff)).replace(hour=0, minute=0,
                                                               second=0)

        #  We request time from api in utc, but using GMT+03... Wrong...
        if {'утро', 'утром'}.intersection(word_set):
            hours = 11
            verbal += ' утром'
        elif {'день', 'днем', 'днём'}.intersection(word_set):
            hours = 16
            verbal += ' днём'
        elif {'вечер', 'вечером'}.intersection(word_set):
            hours = 23
            verbal += ' вечером'
        elif {'ночь', 'ночью'}.intersection(word_set):
            hours = 25
            verbal += ' ночью'
        else:
            hours = 16
            verbal += ' днём'

        return verbal, target_date + timedelta(hours=hours)

    def verbal_weather(self, message):
        print('Message:', message)
        message = message.lower().translate(
            str.maketrans(string.punctuation, ' ' * len(string.punctuation)))
        words = message.split()

        verbal, target_time = self.get_time(words)

        try:
            fc = self.owm.three_hours_forecast(words[0])  # type: Forecaster
        except APICallError:
            fc = None

        if fc is None:
            answer = 'На Земле такого города нет'
        else:
            w = fc.get_weather_at(target_time)  # type: Weather
            print(w)
            stat = w.get_detailed_status().capitalize()
            temp = w.get_temperature('celsius')
            pattern = '{} в городе {}:\n' \
                      '{}\n' \
                      'Температура: {} \N{DEGREE SIGN}C'
            answer = pattern.format(verbal, words[0].capitalize(), stat,
                                    temp['temp'])

        print('Answer:', answer, sep='\n')
        return answer

    def query(self, bot: tg.Bot, update: tg.Update):
        message = update.message.text
        answer = self.verbal_weather(message)
        bot.sendMessage(update.message.chat_id, text=answer)

    weekdays = {
        'понедельник': 0,
        'вторник': 1,
        'среда': 2,
        'среду': 2,
        'четверг': 3,
        'пятница': 4,
        'пятницу': 4,
        'суббота': 5,
        'субботу': 5,
        'воскресенье': 6,
        'воскресение': 6,
    }

    verbal_weekdays = {
        0: 'В понедельник',
        1: 'Во вторник',
        2: 'В среду',
        3: 'В четверг',
        4: 'В пятницу',
        5: 'В субботу',
        6: 'В воскресенье',
    }


def start(bot: tg.Bot, update: tg.Update):
    bot.sendMessage(update.message.chat_id, text='Hello from GoodWeather!')


def unknown(bot: tg.Bot, update: tg.Update):
    print('Unknown command:', update.message.text)
    bot.sendMessage(chat_id=update.message.chat_id, text='Unknown command')


def main():
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

    with open('goodweather.token') as token_file:
        token = token_file.read()

    updater = Updater(token)
    dispatcher = updater.dispatcher

    weather = WeatherGetter()

    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(MessageHandler(Filters.text, weather.query))
    dispatcher.add_handler(MessageHandler(Filters.command, unknown))

    updater.start_polling()

if __name__ == '__main__':
    main()
